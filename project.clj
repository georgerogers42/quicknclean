(defproject quicknclean "0.4.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.pegdown/pegdown "1.0.2"]
                 [compojure "1.1.1"]
                 [ring "1.1.6"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [hiccup "1.0.1"]
                 [enlive "1.0.1"]
                 [com.cemerick/friend "0.1.2"]
                 [postgresql/postgresql "8.4-702.jdbc4"]]
  :plugins [[lein-ring "0.7.3"]]
  :ring {:handler quicknclean.handler/app}
  :main quicknclean.handler
  :profiles
  {:dev {:dependencies [[ring-mock "0.1.3"]]}}
  :min-lein-version "2.0.0")
