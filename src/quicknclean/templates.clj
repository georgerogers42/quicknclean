(ns quicknclean.templates
  (:use net.cgrand.enlive-html)
  (:import [org.pegdown Extensions PegDownProcessor]))
(defn md [text]
  (.markdownToHtml (PegDownProcessor. Extensions/SUPPRESS_ALL_HTML) (or text "")))
(deftemplate list-r "quicknclean/list.html" [pages]
  [:#list]
  (clone-for [page pages]
             [:a.title]     (do-> (set-attr :href (str "/" (:title page)))
                                  (content (:title page)))
             [:div.content] (-> page :body md html-content)))
(deftemplate edit-r "quicknclean/edit.html" [page]
  [:#title] (do-> (-> page :title content)
                  (set-attr :href (str "/" (:title page))))
  [:#body]  (-> page :body  content))
(deftemplate page-r "quicknclean/page.html" [title page]
  [:#title] (do-> (set-attr :href (str "/" title))
                  (content (str "/" title)))
  [:#body]  (-> page :body md html-content)
  [:#edit]  (do-> (set-attr :href (str title "/edit"))
                  (content "Edit")))
(deftemplate login-r "quicknclean/login.html" [])