(ns quicknclean.handler
  (:gen-class)
  (:use compojure.core
        ring.middleware.stacktrace
        ring.adapter.jetty
        net.cgrand.enlive-html
        quicknclean.templates)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.util.response :as response]
            [clojure.java.jdbc :as sql]
            [cemerick.friend :as friend]
            [cemerick.friend.workflows :as workflows]
            [cemerick.friend.credentials :as creds])
  (:import [org.pegdown Extensions PegDownProcessor]
           [java.sql Date]))
(defonce db
  (delay
   (System/getenv "DATABASE_URL")))
(defn user-db [uname]
    (sql/with-connection (force db)
      (sql/with-query-results res
        ["SELECT username, password FROM app_users WHERE username = (?)"
         uname]
        (assoc (first res) :roles #{::user}))))
(defn app-routes [{login-tpl :login
                   page-tpl  :page
                   edit-tpl  :edit
                   list-tpl  :list}]
  (routes
   (GET "/" []
        (sql/with-connection (force db)
          (sql/with-query-results res
            ["SELECT title, body, posted_on FROM posts ORDER BY posted_on desc"]
            (doseq [_ res])
            (list-tpl res))))
   (GET "/login" []
        (login-tpl))
   (GET "/logout" []
        (login-tpl))
   (friend/logout (POST "/logout" request (response/redirect "/")))
   (GET "/favicon.ico" [] "")
   (GET "/:page" [page]
        (sql/with-connection (force db)
          (sql/with-query-results res
            ["SELECT title, body, posted_on FROM posts WHERE title = (?)" page]
            (page-tpl page (first res)))))
   (GET "/:page/edit" [page]
        (sql/with-connection (force db)
          (sql/with-query-results res
            ["SELECT title, body, posted_on FROM posts WHERE title = (?)" page]
            (edit-tpl (first res)))))
   (POST "/signup" [username password]
         (try 
           (sql/with-connection (force db)
             (sql/insert-records :app_users
                                 {:username username :password (creds/hash-bcrypt password)}))
           (response/redirect "/")
           (catch Exception e
             (response/redirect "/login"))))
   (POST "/:page/edit" [page body]
         (friend/authenticated
          (do
            (sql/with-connection (force db)
              (sql/update-or-insert-values
               :posts
               ["title=?" page]
               {:title page :body body :posted_on (Date. (System/currentTimeMillis))}))
            (response/redirect (str "/" page)))))
   (route/not-found "Not Found")))
(def app* (app-routes {:login login-r :page page-r :list list-r :edit edit-r}))
(def app
  (-> app*
      (friend/authenticate {:credential-fn
                            (partial creds/bcrypt-credential-fn user-db)
                            :workflows [(workflows/interactive-form)]})
      (handler/site)))
(defn start [p]
  (run-jetty (wrap-stacktrace #'app) {:port p :join? false}))
(defn -main []
  (run-jetty #'app {:port (or (Integer/parseInt (System/getenv "PORT"))
                              8080)}))