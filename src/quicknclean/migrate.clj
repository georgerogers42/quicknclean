(ns quicknclean.migrate
  (:require [clojure.java.jdbc :as sql]
            [cemerick.friend.credentials :as creds]
            [quicknclean.handler :as app])
  (:gen-class))
(defn -main [username password]
  (sql/with-connection (force app/db)
    (sql/transaction
      (sql/create-table
       :app_users
       [:id "serial primary key"]
       [:username "varchar(50) unique not null"]
       [:password "varchar(255) unique not null"])
      (sql/create-table
       :posts
       [:id "serial primary key"]
       [:title "varchar(50) unique not null"]
       [:body "text not null"]
       [:posted_on "timestamp with timezone not null"])
      (sql/insert-records :app_users {:username username
                                      :password (creds/hash-bcrypt password)}))))