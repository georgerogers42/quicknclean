(ns quicknclean.test.handler
  (:use clojure.test
        ring.mock.request  
        quicknclean.handler))

(deftest test-app
  (testing "main route"
    (let [response (app (request :get "/"))]
      (is (= (:status response) 200))
      (is (re-seq #"/index" (apply str (:body response))))))
  (testing "not-found route"
    (let [response (app (request :get "/invalid/page"))]
      (is (= (:status response) 404)))))
